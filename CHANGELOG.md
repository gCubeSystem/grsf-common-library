# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0]

- Adding support for "FAO SDG 14.4.1 Questionnaire" source [#23670] 
- Upgraded gcube-bom version to 2.1.0

## [v1.3.2]

- Updated Labels [#23167]


## [v1.3.1] - 2021-04-09

### Changed

- Switched dependency management to gcube-bom 2.0.0
[#19500#note-2] Fixing GWT import issue 

## [v1.3.0] [r4.24.0] - 2020-06-19

### Changed

- [#19166] Added support for GRSF_pre VRE with the behaviour of GRSF Admin

## [v1.2.0] - 2019-11-12

### Changed

- Fixed project gcube rule compliancy

## [v1.1.0] - 2019-11-06

### Changed

- Switched to git/jenkins

## [v1.1.0] - 2019-09-28

### Changed

- Switched maven parent

## [v1.0.3] - 2018-07-18

### Added

**Features**

- [#11749] Added 'With Similarities'-'No Similarities' tag to GRSF Records

- [#11766] Added 'Connected'-'Not Connected' tag to GRSF Records

- [#11767] Add group for SDG flag

- [#11811] Added citation field

- [#11968] Changed 'State and trend of Marine Resource' to 'State and Trend'

- [#11969] Changed 'Scientific advice' to 'Scientific Advice'

## [v1.0.2] - 2018-03-21

### Changed

**Fixes**

- [#11487] Fixing issue

## [v1.0.1] - 2017-12-23

### Changed

**Fixes**

- Minor fixes

## [v1.0.0] - 2017-11-05

- First Release
