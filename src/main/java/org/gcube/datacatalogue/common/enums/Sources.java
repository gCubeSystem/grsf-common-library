package org.gcube.datacatalogue.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Source Group and sub groups (for both Stock and Fishery) -> look at "Database Sources"
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public enum Sources {

	FIRMS("FIRMS","firms"),
	RAM("RAM","ram"),
	FISHSOURCE("FishSource", "fishsource"),
	GRSF("GRSF", "grsf"),
	SDG("FAO SDG 14.4.1 Questionnaire","sdg");

	private String sourceName;
	private String urlPath;

	private Sources(String sourceName, String urlPath) {
		this.sourceName = sourceName;
		this.urlPath = urlPath;
	}

	/**
	 * Return the original name
	 * @return
	 */
	public String getSourceName(){
		return sourceName;
	}

	@JsonValue
	public String onSerialize(){
		return urlPath;
	}

	@JsonCreator
	public static Sources onDeserialize(String sourceString) {
		if(sourceString != null) {
			for(Sources source : Sources.values()) {
				if (source.urlPath.equalsIgnoreCase(sourceString.trim())) {
					return source;
				}
				if (source.sourceName.equalsIgnoreCase(sourceString.trim())) {
					return source;
				}
			}
		}
		return null;
	}
	
	public String getURLPath() {
		return urlPath;
	}
	
	@Override
	public String toString() {
		return urlPath;
	}
	
	public static String getJsonArrayAsString(){
		return "[" + FIRMS.urlPath  + "," + 
				RAM.urlPath + "," +  
				FISHSOURCE.urlPath + "," + 
				GRSF.urlPath + "," +  
				SDG.urlPath + "," + "]";
	}
	
	public static List<String> listNames(){
		
		List<String> valuesString = new ArrayList<String>(Sources.values().length);
		for(Sources source : Sources.values()) {
			valuesString.add(source.getSourceName());
		}
		
		return valuesString;
	}

}
